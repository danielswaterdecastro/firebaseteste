/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import firebase from 'firebase';
import {
  Platform,
  StyleSheet,
  Text,
  Button,
  View
} from 'react-native';

export default class App extends Component {

  // constructor(props){
  //   super(props);
  //   this.state = {pontuacao: 0}
  // }
  
  componentWillMount(){
    var config = {
      apiKey: "AIzaSyDARXVp6Nr5ol3Z_fszbeQbj2TnUwXd1jI",
      authDomain: "configuracaofirebasereac-61369.firebaseapp.com",
      databaseURL: "https://configuracaofirebasereac-61369.firebaseio.com",
      projectId: "configuracaofirebasereac-61369",
      storageBucket: "configuracaofirebasereac-61369.appspot.com",
      messagingSenderId: "638093301910"
    };
    firebase.initializeApp(config);
  }
  
  // salvarDados(){
  //   // Salvando os dados, coloca o metodo 'set' apos o metodo 'ref'
  //   // Atualizar os dados, so trocar o valor do metodo 'set'
  //   // Para remover, trocar o metodo set, pelo metodo remove()
  //   //database.ref("pontuacao").remove();

  //   var funcionarios = firebase.database().ref("funcionarios");

  //   funcionarios.push().set( {
  //     nome: 'Dina',
  //     altura: '1.50',
  //     peso: '50kg'
  //   } );
  // }

  // listarDados(){
  //   var pontuacao = firebase.database().ref("pontuacao");
  //   // metodo 'on' adicina um ouvinte
  //   // 'value' fica ouvindo qualquer mudança
  //   pontuacao.on('value', (snapshot)=> {
  //     var pontos = snapshot.val();
  //     this.setState({pontuacao: pontos})
  //   });
  // }

  cadastrarUsuario(){

    var email = "danielswater@gmail.com";
    var senha = "123456";

    const usuario = firebase.auth();

    usuario.createUserWithEmailAndPassword(email, senha)
    .catch( (erro)=> {
      // erro.code, erro.message
      alert(erro.message);
    } );
  }

  verificarUsuarioLogado(){
    const usuario = firebase.auth();

    usuario.onAuthStateChanged(
      (usuarioAtual) => {
        if(usuarioAtual){
          alert('usuario logado');
        }
        else{
          alert('usuario nao esta logado')
        }
      }
    );
    // const usuarioAtual = usuario.currentUser;

    // if(usuarioAtual){
    //   alert('usuario logado');
    // }
    // else{
    //   alert('usuario nao esta logado')
    // }
  }

  deslogarUsuario(){
    const usuario = firebase.auth();
    usuario.signOut();
  }

  logarUsuario(){
    var email = "danielswater@gmail.com";
    var senha = "123456";

    const usuario = firebase.auth();

    usuario.signInWithEmailAndPassword(email, senha)
    .catch( (erro)=> {
      alert(erro.message);
    } )
  }
  
  render() {
    // let {pontuacao} = this.state;
    return (
      <View>
          <View>
              <Button
              onPress={ ()=> { this.cadastrarUsuario() } }
              color="blue"
              title="Cadastrar usuário"/>

              <Button
              onPress={ ()=> { this.verificarUsuarioLogado() } }
              color="blue"
              title="Verificar usário logado"/>

              <Button
              onPress={ ()=> { this.deslogarUsuario() } }
              color="blue"
              title="Deslogar usuario"/>

              <Button
              onPress={ ()=> { this.logarUsuario() } }
              color="blue"
              title="Logar usuario"/>
  
          </View>      
          <View>
              <Text></Text>
          </View>      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
});
